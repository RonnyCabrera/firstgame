//
//  ItemManager.swift
//  Game
//
//  Created by Ronny Cabrera on 7/17/18.
//  Copyright © 2018 USRDEL. All rights reserved.
//

import Foundation
import RealmSwift

class ItemManager {
    
    var scoreItems:[Item] = []
    
    var realm:Realm
    init() {
        realm = try! Realm()
        print (realm.configuration.fileURL)
    }
    
    func addItem(score: String) {
        let item = Item()
        item.id = "\(UUID())"
        item.score = score
        
        try! realm.write {
            realm.add(item)
        }
    }
    
    func updateArrays() {
        scoreItems = Array(realm.objects(Item.self))
    }
}
