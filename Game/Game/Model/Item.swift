//
//  Item.swift
//  Game
//
//  Created by Ronny Cabrera on 7/17/18.
//  Copyright © 2018 USRDEL. All rights reserved.
//

import Foundation
import RealmSwift

class Item: Object {
    
    @objc dynamic var id:String?
    @objc dynamic var score:String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
