//
//  ViewController.swift
//  Game
//
//  Created by USRDEL on 25/4/18.
//  Copyright © 2018 USRDEL. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    @IBOutlet weak var gameSlider: UISlider!
    
    let itemManager = ItemManager()
    var target = 0
    var scoreFinal = 0
    var roundGame = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        restartGame()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func playButtonPressed(_ sender: Any) {
        let sliderValue = Int(gameSlider.value)
        switch sliderValue {
        case target:
            scoreFinal += 100
        case (target - 2)...(target + 2):
            scoreFinal += 50
        case (target - 5)...(target + 5):
            scoreFinal += 10
        default:
            break
        }
        
        roundGame += 1
        target = Int(arc4random_uniform(100))
        
        scoreLabel.text = "\(scoreFinal)"
        targetLabel.text = "\(target)"
        roundLabel.text = "\(roundGame)"
        
        print("Antes de Guardar")
        itemManager.addItem(score: String(scoreFinal))
    }
    
    @IBAction func restartButtonPressed(_ sender: Any) {
        restartGame()
    }
    
    @IBAction func infoButtonPressed(_ sender: Any) {
    }
    
    @IBAction func winnerButtonPressed(_ sender: Any) {
        if(scoreFinal > 100) {
            performSegue(withIdentifier: "toWinnerSegue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toScoreSegue" {
            
            let destination = segue.destination as! ScoreViewController
            destination.itemManager = itemManager
            
        }
    }
    
    @IBAction func scoreButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "toScoreSegue", sender: self)
    }
    
    private func restartGame() {
        target = Int(arc4random_uniform(100))
        scoreFinal = 0
        roundGame = 1
        scoreLabel.text = "0"
        targetLabel.text =  "\(arc4random_uniform(100))"
        roundLabel.text = "1"
    }

}

